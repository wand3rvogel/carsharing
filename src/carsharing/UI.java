package carsharing;

import carsharing.model.Car;
import carsharing.model.Company;
import carsharing.model.Customer;
import carsharing.model.Nameable;
import carsharing.service.CarService;
import carsharing.service.CompanyService;
import carsharing.service.CustomerService;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@RequiredArgsConstructor
public class UI {

    private static final Scanner scanner = new Scanner(System.in);
    private final CustomerService customerService;
    private final CompanyService companyService;
    private final CarService carService;

    public void mainMenu() {

        while (true) {
            new Menu("Log in as a manager",
                    "Log in as a customer",
                    "Create a customer")
                    .withBack("0. Exit").show();

            switch (scanner.nextLine()) {
                case "1" -> managerMenu();
                case "2" -> customerMenu();
                case "3" -> customerService.createCustomer(getNameFor("customer"));
                case "0" -> {
                    return;
                }
            }
        }
    }

    public String getNameFor(String entityName) {
        System.out.printf("%nEnter the %s name:%n", entityName);
        return scanner.nextLine();
    }

    private void managerMenu() {

        while (true) {
            new Menu("Company list",
                    "Create a company").show();

            switch (scanner.nextLine()) {
                case "1" -> companiesMenu();
                case "2" -> companyService.createCompany(getNameFor("company"));
                case "0" -> {
                    return;
                }
            }
        }
    }

    public void companiesMenu() {
        chooseCompany().ifPresentOrElse(this::carMenu,
                () -> System.out.println("\nThe company list is empty!"));
    }

    public Optional<Company> chooseCompany() {

        List<Company> companies = companyService.getAllCompanies();
        return companies.isEmpty() ?
                Optional.empty() :
                selectEntity("Choose the company:", companies);
    }

    public <T extends Nameable> Optional<T> selectEntity(String prompt, List<T> list) {

        new Menu(prompt, list.stream().map(T::getName).toList()).show();
        return getIndexFromChoice(scanner.nextLine())
                .map(list::get);
    }

    private Optional<Integer> getIndexFromChoice(String inputString) {
        if ("0".equals(inputString)) {
            return Optional.empty();
        }

        try {
            return Optional.of(Integer.parseInt(inputString) - 1);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    public void carMenu(Company company) {

        while (true) {
            new Menu("Car list", "Create a car")
                    .withTitle("%n'%s' company".formatted(company.getName()))
                    .show();

            switch (scanner.nextLine()) {
                case "1" -> printCarList(company);
                case "2" -> carService.createCar(getNameFor("car"), company);
                case "0" -> {
                    return;
                }
            }
        }
    }

    public void printCarList(Company company) {

        List<Car> cars = carService.getCarsByCompany(company);
        if (cars.isEmpty()) {
            System.out.println("\nThe car list is empty!");
            return;
        }
        new Menu("Car list:", cars.stream().map(Car::getName).toList()).printList();
    }

    public void customerMenu() {

        chooseCustomer().ifPresentOrElse(this::rentMenu,
                () -> System.out.println("\nThe customer list is empty!"));
    }

    public Optional<Customer> chooseCustomer() {

        List<Customer> customers = customerService.getAllCustomers();
        return customers.isEmpty() ?
                Optional.empty() :
                selectEntity("Customer list:", customers);
    }

    public void rentMenu(Customer customer) {
        while (true) {
            new Menu("Rent a car",
                    "Return a rented car",
                    "My rented car").show();

            switch (scanner.nextLine()) {
                case "1" -> rentFreeCar(customer);
                case "2" -> customerService.returnRentedCar(customer);
                case "3" -> customerService.rentedCarInfo(customer);
                case "0" -> {
                    return;
                }
            }
        }
    }

    public void rentFreeCar(Customer customer) {

        if (customer.hasRentedCar()) {
            System.out.println("\nYou've already rented a car!");
            return;
        }

        chooseCompany()
                .flatMap(carService::getFreeCarsByCompany)
                .flatMap(this::selectFreeCar)
                .ifPresent(car -> customerService.rentCar(customer, car));
    }

    public Optional<Car> selectFreeCar(List<Car> carList) {
        return selectEntity("Choose a car:", carList);
    }
}
