package carsharing;

import carsharing.dao.CarDao;
import carsharing.dao.CompanyDao;
import carsharing.dao.CustomerDao;
import carsharing.service.CarService;
import carsharing.service.CompanyService;
import carsharing.service.CustomerService;
import carsharing.util.InitializeDatabase;

import java.util.Optional;

public class Main {

    public static final String DEFAULT_DB_NAME = "carsharing";

    public static void main(String[] args) {
        new Main().run(args);
    }

    public void run(String[] args) {

        String dbName = getDbName(args).orElse(DEFAULT_DB_NAME);
        InitializeDatabase.start(dbName);

        CarService carService = new CarService(new CarDao(dbName));
        CompanyService companyService = new CompanyService(new CompanyDao(dbName));
        CustomerService customerService = new CustomerService(new CustomerDao(dbName), companyService, carService);

        new UI(customerService, companyService, carService).mainMenu();
    }

    private Optional<String> getDbName(String[] args) {
        boolean isSpecifiedDbName = args.length > 1
                                    && "-databaseFileName".equals(args[0])
                                    && !args[1].isEmpty();
        return isSpecifiedDbName ? Optional.of(args[1]) : Optional.empty();
    }
}