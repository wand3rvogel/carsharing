package carsharing;

import java.util.List;
import java.util.stream.IntStream;

public class Menu {

    String title = "";
    List<String> items;
    String backItem = "0. Back";

    public Menu(String title, List<String> items) {
        this.title = title;
        this.items = items;
    }

    public Menu(String... items) {
        this.items = List.of(items);
    }

    public void printList() {

        System.out.println();
        if (!title.isEmpty()) {
            System.out.println(title);
        }
        IntStream.range(0, items.size()).forEach(i -> System.out.printf("%d. %s%n", i + 1, items.get(i)));
    }

    public void show() {
        printList();
        System.out.println(backItem);
    }

    public Menu withTitle(String title) {
        this.title = title;
        return this;
    }

    public Menu withBack(String backString) {
        this.backItem = backString;
        return this;
    }
}
