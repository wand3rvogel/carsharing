package carsharing.dao;

import carsharing.util.ConnectionManager;
import carsharing.model.Customer;
import lombok.RequiredArgsConstructor;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class CustomerDao {

    private final String dbName;

    public void persist(Customer customer) {

        String sql = "INSERT INTO CUSTOMER (name) VALUES (?)";

        try (Connection conn = ConnectionManager.open(dbName);
             PreparedStatement prepStmt = conn.prepareStatement(sql)) {

            prepStmt.setString(1, customer.getName());
            prepStmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void update(Customer customer) {

        String sql = "UPDATE CUSTOMER SET RENTED_CAR_ID = ? WHERE ID = ?";

        try (Connection conn = ConnectionManager.open(dbName);
             PreparedStatement prepStmt = conn.prepareStatement(sql)) {

            if (customer.hasRentedCar()) {
                prepStmt.setInt(1, customer.getRentedCarId());
            } else {
                prepStmt.setNull(1, Types.INTEGER);
            }
            prepStmt.setInt(2, customer.getId());
            prepStmt.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Customer> findAll() {

        String Sql = "SELECT * FROM CUSTOMER ORDER BY ID;";

        try (Connection conn = ConnectionManager.open(dbName);
             Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(Sql);

            List<Customer> customers = new ArrayList<>();
            while (rs.next()) {
                int customerId = rs.getInt("id");
                String customerName = rs.getString("name");
                Integer rentedCarId = rs.getInt("rented_car_id");
                if (rs.wasNull()) {
                    rentedCarId = null;
                }
                customers.add(new Customer(customerId, customerName, rentedCarId));
            }
            return customers;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
