package carsharing.dao;

import carsharing.util.ConnectionManager;
import carsharing.model.Car;
import lombok.RequiredArgsConstructor;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class CarDao {

    private final String dbName;

    public List<Car> findAllByCompanyId(Integer companyId) {

        String sql = "SELECT * FROM CAR WHERE COMPANY_ID = ? ORDER BY ID;";
        return findByCompanyId(companyId, sql);
    }

    public List<Car> findFreeByCompanyId(Integer companyId) {

        String sql = """
                SELECT car.id, car.name, company_id
                FROM car LEFT JOIN customer c ON car.id = c.rented_car_id
                WHERE company_id = ? AND rented_car_id IS NULL;
                """;

        return findByCompanyId(companyId, sql);
    }

    public void persist(Car car) {

        String sql = "INSERT INTO CAR (NAME, COMPANY_ID) VALUES (?,?)";

        try (Connection conn = ConnectionManager.open(dbName);
             PreparedStatement prepStmt = conn.prepareStatement(sql)) {

            prepStmt.setString(1, car.getName());
            prepStmt.setInt(2, car.getCompanyId());
            prepStmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public Optional<Car> findById(Integer carId) {

        String sql = String.format("SELECT * FROM CAR WHERE ID = %d;", carId);

        try (Connection conn = ConnectionManager.open(dbName);
             Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            return rs.next() ?
                    Optional.of(new Car(rs.getInt("id"), rs.getString("name"), rs.getInt("company_id")))
                    : Optional.empty();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public List<Car> findByCompanyId(Integer companyId, String sqlSelect) {

        try (Connection conn = ConnectionManager.open(dbName);
             PreparedStatement prepStmt = conn.prepareStatement(sqlSelect)) {

            prepStmt.setInt(1, companyId);
            ResultSet rs = prepStmt.executeQuery();

            List<Car> cars = new ArrayList<>();
            while (rs.next()) {
                cars.add(new Car(rs.getInt("id"), rs.getString("name"), rs.getInt("company_id")));
            }
            return cars;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
