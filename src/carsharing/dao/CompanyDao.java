package carsharing.dao;

import carsharing.util.ConnectionManager;
import carsharing.model.Company;
import lombok.RequiredArgsConstructor;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class CompanyDao {

    private final String dbName;

    public List<Company> findAll() {

        String Sql = "SELECT * FROM COMPANY ORDER BY ID;";

        try (Connection conn = ConnectionManager.open(dbName);
             Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(Sql);

            List<Company> companies = new ArrayList<>();
            while (rs.next()) {
                companies.add(new Company(rs.getInt("id"), rs.getString("name")));
            }
            return companies;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void persist(Company company) {

        String sql = "INSERT INTO COMPANY (name) VALUES (?)";

        try (Connection conn = ConnectionManager.open(dbName);
             PreparedStatement prepStmt = conn.prepareStatement(sql)) {

            prepStmt.setString(1, company.getName());
            prepStmt.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<Company> findById(Integer companyId) {

        String sql = String.format("SELECT * FROM COMPANY WHERE ID = %d;", companyId);

        try (Connection conn = ConnectionManager.open(dbName);
             Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            return rs.next() ?
                    Optional.of(new Company(rs.getInt("id"), rs.getString("name")))
                    : Optional.empty();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
