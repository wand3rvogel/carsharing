package carsharing.service;

import carsharing.dao.CarDao;
import carsharing.model.Car;
import carsharing.model.Company;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class CarService {

    private final CarDao carDao;

    public List<Car> getCarsByCompany(Company company) {
        return carDao.findAllByCompanyId(company.getId());
    }

    public Optional<List<Car>> getFreeCarsByCompany(Company company) {

        List<Car> freeCars = carDao.findFreeByCompanyId(company.getId());
        if (freeCars.isEmpty()) {
            System.out.println("\nNo available cars in the '" + company.getName() + "' company");
            return Optional.empty();
        }
        return Optional.of(freeCars);
    }

    public void createCar(String name, Company company) {

        carDao.persist(new Car(name, company.getId()));
        System.out.println("The car was added!");
    }

    public Optional<Car> getCarById(Integer carId) {
        return carDao.findById(carId);
    }
}
