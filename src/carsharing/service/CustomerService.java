package carsharing.service;

import carsharing.dao.CustomerDao;
import carsharing.model.Car;
import carsharing.model.Company;
import carsharing.model.Customer;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class CustomerService {

    private final CustomerDao customerDao;
    private final CompanyService companyService;
    private final CarService carService;

    public List<Customer> getAllCustomers() {
        return customerDao.findAll();
    }

    public void createCustomer(String name) {

        customerDao.persist(new Customer(name));
        System.out.println("The customer was added!");
    }

    public void returnRentedCar(Customer customer) {

        if (customer.hasRentedCar()) {
            customer.returnRentedCar();
            customerDao.update(customer);
            System.out.println("\nYou've returned a rented car!");
        } else {
            System.out.println("\nYou didn't rent a car!");
        }
    }

    public void rentedCarInfo(Customer customer) {

        if (customer.hasRentedCar()) {

            System.out.println("Your rented car:");
            Car rentedCar = carService.getCarById(customer.getRentedCarId()).orElseThrow(IllegalStateException::new);
            System.out.println(rentedCar.getName());

            System.out.println("Company:");
            Company company = companyService.getCompanyById(rentedCar.getCompanyId()).orElseThrow(IllegalStateException::new);
            System.out.println(company.getName());
        } else {
            System.out.println("\nYou didn't rent a car!");
        }
    }


    public void rentCar(Customer customer, Car selectedCar) {

        customer.rentCar(selectedCar);
        customerDao.update(customer);

        System.out.printf("%nYou rented '%s'%n", selectedCar.getName());
    }


}
