package carsharing.service;

import carsharing.dao.CompanyDao;
import carsharing.model.Company;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class CompanyService {

    private final CompanyDao companyDao;

    public void createCompany(String name) {

        companyDao.persist(new Company(name));
        System.out.println("The company was created!");
    }

    public List<Company> getAllCompanies() {
        return companyDao.findAll();
    }

    public Optional<Company> getCompanyById(Integer companyId) {
        return companyDao.findById(companyId);
    }
}
