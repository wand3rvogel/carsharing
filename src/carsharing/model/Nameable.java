package carsharing.model;


public interface Nameable {

    String getName();
}
