package carsharing.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Car implements Nameable {

    private Integer id;
    private final String name;
    private Integer companyId;

    public Car(String name, Integer companyId) {
        this.name = name;
        this.companyId = companyId;
    }
}
