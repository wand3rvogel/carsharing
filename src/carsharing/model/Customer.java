package carsharing.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Customer implements Nameable {

    private Integer id;
    private String name;
    private Integer rentedCarId = null;

    public Customer(String name) {
        this.name = name;
    }

    public boolean hasRentedCar() {
        return this.rentedCarId != null;
    }

    public void rentCar(Car car) {
        this.rentedCarId = car.getId();
    }

    public void returnRentedCar() {
        this.rentedCarId = null;
    }
}
