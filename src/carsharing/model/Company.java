package carsharing.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Company implements Nameable {

    private Integer id;
    private final String name;

    public Company(String name) {
        this.name = name;
    }
}


