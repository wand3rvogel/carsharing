package carsharing.util;

import java.sql.*;

public final class ConnectionManager {

    private static final String DB_PATH = "jdbc:h2:./src/carsharing/db/";

    private ConnectionManager() {
    }

    public static Connection open(String dbName) {

        try {
            Connection connection = DriverManager.getConnection(DB_PATH + dbName);
            connection.setAutoCommit(true);
            return connection;

        } catch (SQLException ex) {
            throw new RuntimeException("Error connecting to the database", ex);
        }
    }
}
