package carsharing.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public final class InitializeDatabase {

    private InitializeDatabase() {}

    public static void start(String dbName) {

        final String SQL_CREATE_TABLE_COMPANY = """
                CREATE TABLE IF NOT EXISTS COMPANY (
                    id INT PRIMARY KEY AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL UNIQUE
                );
                """;

        final String SQL_CREATE_TABLE_CAR = """
                CREATE TABLE IF NOT EXISTS CAR (
                    id INT PRIMARY KEY AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL UNIQUE,
                    company_id INT NOT NULL,
                    CONSTRAINT fk_company FOREIGN KEY (company_id)
                    REFERENCES COMPANY(id)
                );
                """;

        final String SQL_CREATE_TABLE_CUSTOMER = """
                CREATE TABLE IF NOT EXISTS CUSTOMER (
                    id INT PRIMARY KEY AUTO_INCREMENT,
                    name VARCHAR(255) NOT NULL UNIQUE,
                    rented_car_id INT,
                    CONSTRAINT fk_car FOREIGN KEY (rented_car_id)
                    REFERENCES CAR(id)
                );
                """;
        try (Connection conn = ConnectionManager.open(dbName);
             Statement stmt = conn.createStatement()) {

            stmt.executeUpdate(SQL_CREATE_TABLE_COMPANY);
            stmt.executeUpdate(SQL_CREATE_TABLE_CAR);
            stmt.executeUpdate(SQL_CREATE_TABLE_CUSTOMER);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
